<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('accueil');
});

Route::get('/mon_compte', function () {
    return view('mon_compte');
});

Route::get('/mes_reservations', function () {
    return view('mes_reservations');
});

Route::get('/chocolats', function (){
    return view('chocolats');
});

Route:: get('/a_propos', function () {
    return view('a_propos');
});

Route::get('/reservation', function () {
    return view('reservation');
});