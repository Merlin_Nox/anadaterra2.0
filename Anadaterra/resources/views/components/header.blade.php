<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Anandaterra</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="public/css/app.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
    <header>
    <nav class="navbar">
        <a class="navbar-brand" href="{{ ('/') }}">
        <b>ANANDA</b>TERRA
            <!-- <img src="" width="30" height="30" class="d-inline-block align-top" alt="logo"> -->
        </a>
        <div>
        <a href="{{('/mon_compte') }}">Mon compte</a>
        <a href="{{('/mes_reservations') }}">Mes réservations</a>
        </div>
    </nav>
    <ul class="nav justify-content-center align-items-center">
    <li class="nav-item">
            <a href="{{ ('/') }}">Accueil</a>
        </li>
        <li class="nav-item">
            <a href="{{ ('/chocolats') }}">Chocolats</a>
        </li>
        <li class="nav-item">
            <a href="{{ ('/a_propos') }}">A propos</a>
        </li>
        <li class="nav-item">
        <a href="{{ ('/reservation')}}" class="btn btn-primary">JE RESERVE</a>
        </li>
    </ul>
    </header>