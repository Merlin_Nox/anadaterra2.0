@include('components/header')
<main>
    <section class="text-center">
        <h2>CHOCOLAT CRU ARIEGE</h2>
        <h3>L'aliment des dieux</h3>
        <h1>BIENVENUE</h1>
    </section>

    <section class="card-container d-flex justify-content-center">
        <div class="card text-center" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title">Chocolat cru</h5>
                <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit ea officia enim voluptas dignissimos facere reiciendis!</p>
                <a href="#" class="btn btn-primary">Voir le produit</a>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="col">
                <img src="" alt="cacaoseed">
            </div>
            <div class="col text-center">
                <h3><u>C'EST QUOI LE CHOCOLAT CRU ?</u></h3>
                <p>C'est du chocolat à base de cacao, de beurre de cacao mélangé avec de l'huile de coco accompagné de miel ou de dattes pour attenuer l'amertume du cacao. Ce qui change en comparaison au chocolat "normal" c'est qu'aucun des ingrédients n'est chauffé à plus de 42° ainsi que l'absence de produits animaliers (excepté le miel).</p>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h3><u>POURQUOI CRU ?</u></h3>
                <p>C'est simplement afin de conserver tous les bienfaits des composants présents dans les ingrédients cités des bienfaits tels que la présence de puissants antioxydants qui empêchent nos cellules de vieillir prématurément. Nous pourrions également citer les multiples vitamines bénéfiques à notre corps... En plus des bienfaits intenses pour notre corps, la synergie des goûts  que procurent les ingrédients crus sont très savoureux et restent plus longtemps en bouche. ...Mais à quoi bon intellectualiser une telle chose ? Découvrez la différence par vous-même !</p>
            </div>
            <div class="col">
                <img src="" alt="cacaoseed">
            </div>
        </div>
    </section>

    <section class="text-center">
        <h1>LE SAVIEZ-VOUS ?</h1>
        <p>Théobromine est un mot dérivé de "Theobroma", nom générique du cacaoyer composé des racines grecques Theo (dieu) et broma (nourriture) signifiant "Nourriture des dieux". Rapprochez vous de votre nature divine en consommant du chocolat cru ;) </p>
    </section>

    <section class="text-center">
        <div class="row">
            <div class="col">
                <h1>Sélection</h1>
            </div>
            <div class="col">
                <h1>Réservation</h1>
            </div>
            <div class="col">
                <h1>Livraison</h1>
            </div>
        </div>
    </section>

    <section>
        <h1 class="text-center">LES INGREDIENTS</h1>
        <div class="row">
            <div class="col">
                <h2>Poudre et beurre de cacao</h2>
                <p>La poudre et le beurre de cacao que nous utilisons proviennent d'un ferme familiale située au Pérou. Certifiée AB, elle possède également le label commerce équitable et vegan.</p>
                <h2>Dattes</h2>
                <p>Pâte de datte biologiques d'origine Tunisienne.</p>
            </div>
            <div class="col d-flex justify-content-end">
                <div>
                    <h2>Miel</h2>
                    <p>Miel biologique de préférence issu de producteurs locaux.</p>
                    <h2>Huile de coco</h2>
                    <p>Produit biologique d'origine Philippine</p>
                </div>
            </div>
        </div>
    </section>

    <section class="text-center">
        <h1>Nos chocolats</h1>
        <a href="#">Vous êtes invités à consulter les tarifs en version pdf</a>
    </section>

</main>
    
@include('components/footer')
