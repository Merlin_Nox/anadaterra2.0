<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleHasClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles_has__clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articles_id')->unsigned('id');
            $table->foreign('articles_id')->references('id')->on('articles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('users_id')->unsigned('id');
            $table->foreign('users_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->decimal('quantité');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_has__clients');
    }
}
