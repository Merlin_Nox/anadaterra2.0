<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
 
        $articles = Article::all();
 
        return view('accueil', ['articles' => $articles]);
    }

    public function index2(){
 
        $articles = Article::all();
 
        return view('admin/index', ['articles' => $articles]);
    }

    public function seeAllChocolats() {

        $articles = Article::all();
 
        return view('chocolats', ['articles' => $articles]);
    }
 
    public function create(){
        
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'price'=> 'required',
            'description' => 'required',
            'image' =>'required',
          ]);
          $article = new Article([
            'name' => $request->get('name'),
            'price'=> $request->get('price'),
            'description'=> $request->get('description'),
            'image' =>$request->get('image'),
          ]);
          $article->save();
          return redirect('/index')->with('success', 'Le produit a bien été ajouté !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('details-choco',['article' => Article::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('admin.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'price'=> 'required',
            'description' => 'required',
            'image' =>'required',
        ]);

        $article = Article::find($id);
        $article->name = $request->get('name');
        $article->price = $request->get('price');
        $article->description = $request->get('description');
        $article->image = $request->get('image');
        $article->save();
    
          return redirect('/index')->with('success', 'Le produit a bien été mis à jour !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

     return redirect('/index')->with('success', 'Le produit a bien été supprimé !');
    }

}
