<?php

namespace App\Http\Controllers;

use App\Article_has_Client;
use Illuminate\Http\Request;

class ArticleHasClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article_has_Client  $article_has_Client
     * @return \Illuminate\Http\Response
     */
    public function show(Article_has_Client $article_has_Client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article_has_Client  $article_has_Client
     * @return \Illuminate\Http\Response
     */
    public function edit(Article_has_Client $article_has_Client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article_has_Client  $article_has_Client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article_has_Client $article_has_Client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article_has_Client  $article_has_Client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article_has_Client $article_has_Client)
    {
        //
    }
}
