@include('components/header')
<div class="card-columns">
    @foreach($articles as $article)
    <div class="card"  style="width:18rem;">
        <img class="card-img-top" src="{{$article->image}}" alt="">
        <div class="card-body">
            <div class="card-title">{{$article->title}}</div>
            <div class="card-text">{{$article->description}}</div>
        </div>
        <div class="card-footer">
            {{$article->price}} €
        </div>
    </div>
    @endforeach
</div>
@include('components/footer')
