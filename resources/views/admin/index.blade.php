@extends('layouts.app')

@section('content')
<div>
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr class="text-center">
          <td>ID</td>
          <td>Nom du Produit</td>
          <td>Prix a l'unité</td>
          <td>Description</td>
          <td>Photo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
    @foreach($articles as $article)
        <tr class="text-center">
            <td>{{ $article->id }}</td>
            <td>{{ $article->name }}</td>
            <td>{{ $article->price }}</td>
            <td>{{ $article->description }}</td>
            <td><img src="{{url($article->image)}}" alt=""></td>
            <td><a href="{{ route('admin.edit',$article->id)}}" class="btn btn-primary">Modifier</a></td>
            <td>
                <form action="{{ route('admin.destroy', $article->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Supprimer</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
  </table>
<div>

<a href="{{ route('admin.create') }}" class="btn btn-primary btn-lg btn-block">AJOUTER UN PRODUIT</a>
@endsection