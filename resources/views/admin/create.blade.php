@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header">
    Ajouter un Produit !
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('admin.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nom du Produit :</label>
              <input type="text" class="form-control" name="name" id="name"/>
          </div>
          <div class="form-group">
              <label for="description">Description :</label>
              <input type="text" class="form-control" name="description" id="description"/>
          </div>
          <div class="form-group">
              <label for="image">Url de l'image du produit :</label>
              <input type="text" class="form-control" name="image" id="image"/>
          </div>
          <div class="form-group">
              <label for="unit_price">Prix a l'unité (en euros) :</label>
              <input type="number" class="form-control" name="price" id="price"/>
          </div>
          <button type="submit" class="btn btn-primary">Ajouter</button>
      </form>
  </div>
</div>
@endsection