<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Anandaterra</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href={{asset('css/app.css')}}>
    </head>
    <body>
    <header>
        <nav class="navbar topbar">
            <a class="navbar-brand title" href="{{ ('/') }}">
            <b>ANANDA</b>TERRA
                <img src="https://image.noelshack.com/fichiers/2019/36/5/1567770076-600.png" width="70" height="70" alt="logo">
            </a>
            <div>
                @guest
                <a href="{{ route('login') }}" class="light">Connexion</a>
                <a href="{{ route('register') }}" class="light">Inscription</a>
                @else
                <a href="{{('/home') }}" class="light">Mon compte</a>
                <!-- <a href="{{('/mes_reservations') }}" class="light">Mes réservations</a> -->
                @endauth
            </div>
        </nav>
        <ul class="nav justify-content-center align-items-center downbar">
            <li class="nav-item">
                <a href="{{ ('/') }}" class="light">Accueil</a>
            </li>
            <li class="nav-item">
                <a href="{{ ('/chocolats') }}" class="light">Chocolats</a>
            </li>
            <li class="nav-item">
                <a href="#a-propos" class="light">A propos</a>
            </li>
            @if (!session('status'))
            <li class="nav-item">
                <a href="{{ url('/')}}" class="btn btn-primary">JE RESERVE</a>
            </li>
            @endif
        </ul>
    </header>