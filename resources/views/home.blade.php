@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="bandeau_gauche">
        <img src="images/nordwood-themes-rloF1agMJmc-unsplash.jpg" class="bandeau" alt="Chococolat">
        <div class="profil">
            <div class="avatar d-flex align-items-center">
                <h3 style="text-transform: uppercase;">{{ Auth::user()->nom}} {{ Auth::user()->prenom}}</h3>
            </div>
            <div class="titre_profil">
                <ul>
                    <a href="#"><li> Mon profil</li></a>
                    <a href="#"><li> Mon panier</li></a>
                    <a href="#"><li> Mes réservations</li></a>
                    @role('admin')
                    <a href="{{ ('/index')}}"><li>Mes produits</li></a>
                    @endrole
                </ul>
            </div>
        </div> 
           
    </div>
    <div class="test">
        <div class="titre_page">
            <h2>Mon profil</h2>
        </div>
        
        <form action="{{ url('users') }}" method="POST">
        {{ csrf_field() }}
            <div class="coordonnées">
                <div>
                    <p>Mot de passe </p>
                    <input type="password" placeholder="************"  disabled="disabled">
                </div>
                <div>
                    <button type="button"  data-toggle="modal" data-target="#modifier_MDP">MODIFIER</button>
                </div>
            </div>
            <div class="coordonnées">
                <div>
                    <p>Numéro de téléphone </p>
                <input type="text"  placeholder={{ Auth::user()->phone}} disabled="disabled">
                </div>
                <div>
                    <button type="button"  data-toggle="modal" data-target="#modifier_tel">MODIFIER</button>
                </div>
            </div>
        </form>
        <div class="modal fade" id="modifier_MDP" tabindex="-1" role="dialog" aria-labelledby="modifier_MDPLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modifier_MDPLabel">Modifier votre mot de passe</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <p>Tapez votre nouveau mot de passe </p>
                            <input type="password" >
                            <p>Confirmez votre nouveau mot de passe</p>
                            <input type="password"  >
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Annuler</button>
                            <button type="button" class="btn btn-outline-success">Enregistrer</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="modifier_tel" tabindex="-1" role="dialog" aria-labelledby="modifier_telLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modifier_telLabel">Modifier votre Numéro de téléphone</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <p>Tapez votre nouveau numero de téléphone </p>
                            <input type="text">
                            <p>Confirmez votre nouveau numero de téléphone</p>
                            <input type="text">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Annuler</button>
                            <button type="button" class="btn btn-outline-success">Enregistrer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
