@include('components/header')

<div class="container-details d-flex justify-content-between" style="margin:30px;">
    <div class="">
        <img src="{{url($article->image)}}" alt="img-article" width="300" height="300">
    </div>
    <div class="d-flex flex-column align-items-center  justify-content-center">
        <h1 class="text-center" >{{ $article->name}}</h1>
        <h5 class="text-center">{{ $article->description}}</h5>
        <p class="text-center"> Prix a l'unité : {{ $article->price }} €</p>
    </div>
</div>

@include('components/footer')